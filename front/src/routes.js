export default {
    '/': {
        name: 'Home',
        auth: false
    },
    '/login': {
        name: 'Login',
        auth: false
    },
    '/transactions': {
        name: 'Transactions',
        auth: true
    }
}
