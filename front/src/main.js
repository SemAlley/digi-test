import Vue from 'vue'
import routes from './routes'
import BootstrapVue from 'bootstrap-vue'

import axios from 'axios';
import VueAxios from 'vue-axios';

import Auth from './core/Auth';

Vue.use(BootstrapVue);

Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = 'http://localhost:8888/api/v1';

Vue.use(Auth);

Vue.component('v-link', require('./components/VLink.vue'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('transactions', require('./components/Transactions.vue'));

const app = new Vue({
    el: '#app',
    data: {
        currentRoute: window.location.pathname
    },
    computed: {
        ViewComponent() {
            let matchingView = routes[this.currentRoute];
            let pageName = matchingView ? matchingView.name : '404';
            //TODO: Remove console.log()
            console.log( this.currentRoute );
            if(matchingView.auth === true && !this.auth().loggedIn()) {
                this.currentRoute = '/login';
                pageName = 'Login';
            }
            return require('./pages/' + pageName + '.vue')
        }
    },
    render(h) {
        return h(this.ViewComponent)
    }
});

window.addEventListener('popstate', () => {
    //TODO: Remove console.log()
    console.log( 123 );
    app.currentRoute = window.location.pathname
});
