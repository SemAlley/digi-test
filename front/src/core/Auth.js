const Auth = {
    _token: null,
    _user: null,
    install(Vue, options) {
        let self = this;
        this.init(Vue);
        Vue.prototype.auth = () => {
            return {
                login(data) {
                    return Vue.axios.post('/auth/login', data)
                        .then(response => {
                            self._token = response.data.access_token;
                            Vue.axios.defaults.headers.common['Authorization'] = 'bearer ' + self._token;
                            localStorage.setItem('jwtToken', self._token);

                        });
                },
                logout() {
                    self._token = null;
                    localStorage.removeItem('jwtToken');
                    Vue.currentRoute = '/login';
                    window.history.pushState(
                        null,
                        'Login',
                        '/login'
                    )
                },
                loggedIn() {
                    return !!self._token;
                },
                getUser() {
                    return new Promise((resolve, reject) => {
                        if(self._user) {
                            resolve(self._user);
                        }
                        Vue.axios.get('/me')
                            .then(response => {
                                self._user = response.data;
                                resolve(self._user);
                            }).catch(err => {
                                reject(err);
                        });
                    })
                }
            }
        }
    },
    init(Vue) {
        if(localStorage.getItem('jwtToken')) {
            this._token = localStorage.getItem('jwtToken');
            Vue.axios.defaults.headers.common['Authorization'] = 'bearer ' + localStorage.getItem('jwtToken');
        }
    }
};

export default Auth;