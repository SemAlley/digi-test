<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->get('/', function () use ($router) {
    return response()->json(['v' => $router->app->version()]);
});

$router->group([
    'prefix'     => 'auth'
], function ($router) {

    $router->post('login', 'AuthController@login');
    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');

});
$router->group([
    'middleware' => 'auth'
], function ($router) {
    $router->get('me', 'AuthController@me');

    /**
     * Customer routes group
     */
    $router->group([
        'prefix' => 'customer'
    ], function ($router) {

        $router->post('/', 'CustomerController@create');
    });

    /**
     * Transaction routes group
     */
    $router->group([
        'prefix' => 'transaction'
    ], function ($router) {

        $router->post('/', 'TransactionController@create');
        $router->get('/{customerId}/{transactionId}', 'TransactionController@getTransaction');
        $router->post('/search', 'TransactionController@getTransactions');
        $router->patch('/{transactionId}', 'TransactionController@update');
        $router->delete('/{transactionId}', 'TransactionController@delete');
    });

});
