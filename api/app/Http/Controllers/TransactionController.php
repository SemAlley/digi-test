<?php

namespace App\Http\Controllers;


use App\Customer;
use App\Transaction;
use DateTime;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Proceed getting a transaction
     *
     * @param $customerId
     * @param $transactionId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransaction($customerId, $transactionId)
    {
        $customer = Customer::find($customerId);
        if ( ! $customer) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong customer ID'
            ], 400);
        }

        $transaction = $customer->transactions()->find($transactionId);
        if ( ! $transaction) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong transaction ID'
            ], 400);
        }

        return response()->json([
            'success'       => true,
            'transactionId' => $transaction->id,
            'amount'        => $transaction->amount,
            'date'          => $transaction->created_at
        ]);
    }

    /**
     * Proceed getting transaction by filters
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactions(Request $request)
    {
        $customerId = $request->get('customerId');
        $amount     = $request->get('amount');
        $date       = $request->get('date');
        $offset     = $request->get('offset') ?? 0;
        $limit      = $request->get('limit') ?? 15;

        if ($customerId) {
            $transactionsQuery = Transaction::where('customer_id', $customerId);
        } else {
            $transactionsQuery = Transaction::whereNotNull('customer_id');
        }
        if ($amount) {
            $transactionsQuery->where('amount', $amount);
        }
        if ($date) {
            $date = date('Y-m-d', strtotime($date));
            $transactionsQuery->whereDate('created_at', $date);
        }
        $transactions = $transactionsQuery->offset($offset)->limit($limit)->get();
        $result       = [];
        foreach ($transactions as $transaction) {
            $result[] = [
                'transactionId' => $transaction->id,
                'customerId'    => $transaction->customer_id,
                'amount'        => $transaction->amount,
                'date'          => $transaction->created_at
            ];
        }

        return response()->json($result);
    }

    /**
     * Proceed adding a transaction
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'customerId' => 'required',
            'amount'     => 'required|numeric'
        ]);

        $customer = Customer::find($request->get('customerId'));
        if ( ! $customer) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong customer ID'
            ], 400);
        }
        $transaction = new Transaction($request->only('amount'));
        $customer->transactions()->save($transaction);

        return response()->json([
            'success'       => true,
            'transactionId' => $transaction->id,
            'customerId'    => $customer->id,
            'amount'        => $transaction->amount,
            'date'          => $transaction->created_at
        ]);
    }

    /**
     * Proceed updating a transaction
     *
     * @param Request $request
     * @param $transactionId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $transactionId)
    {
        $this->validate($request, [
            'amount' => 'required|numeric'
        ]);
        $transaction = Transaction::find($transactionId);
        if ( ! $transaction) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong transaction ID'
            ], 400);
        }
        $transaction->amount = $request->get('amount');
        $transaction->save();

        return response()->json([
            'success'       => true,
            'transactionId' => $transaction->id,
            'customerId'    => $transaction->customer_id,
            'amount'        => $transaction->amount,
            'date'          => $transaction->created_at
        ]);
    }

    /**
     * Proceed deleting a transaction
     *
     * @param $transactionId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($transactionId)
    {
        $transaction = Transaction::find($transactionId);
        if ( ! $transaction) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong transaction ID'
            ], 400);
        }

        return response()->json([
            'success' => $transaction->delete()
        ]);
    }
}
