<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Proceed adding of a customer
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'cnp' => 'required'
        ]);

        $customer = new Customer($request->only('name', 'cnp'));
        $customer->save();

        return response()->json([
            'success' => true,
            'customerId' => $customer->id
        ]);
    }
}
