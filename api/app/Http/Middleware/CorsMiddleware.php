<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 01.04.18
 * Time: 22:43
 */

namespace App\Http\Middleware;


use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->header('Access-Control-Allow-Origin','*');
        $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));

        return $response;
    }
}