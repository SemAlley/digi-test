<?php

namespace App\Console;

use App\Report;
use App\Transaction;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\KeyGenerateCommand',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $prevDay      = date('Y-m-d', strtotime("-1 days"));
            $transactions = Transaction::whereDate('created_at', $prevDay)->get();
            $sum          = 0;
            foreach ($transactions as $transaction) {
                $sum += $transaction->amount;
            }

            $report = new Report(['sum' => $sum, 'target' => $prevDay]);
            $report->save();
            \Log::debug('New report created', [$sum, $prevDay]);
        })->cron('47 23 */2 * *');
    }
}
