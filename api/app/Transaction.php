<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['amount'];

    /**
     * Get the customer owns the transaction
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}