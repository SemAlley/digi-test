# DIGI Test task

## Install

### If use included docker:

* Run 
```
docker-compose build
```

### If custom

* Set up lumen project in **/api** folder
* Set up front-end project in **/front** folder. 
Change /front/src/main.js :
```
Vue.axios.defaults.baseURL = 'http://localhost:8888/api/v1';
```
to
```
Vue.axios.defaults.baseURL = 'http://{your_api_domain}/api/v1';
```

## Setup

# API
Create .env file in /api dir (for docker install u can use .env.my)
```
cd project_path/api
compose install
php artisan migrate
php artisan db:seed # To create admin user
```

#Front
```
cd project_path/front
npm install
npm run build
```

## Cron

Cron is set up to docker. For custom install use web/cron command